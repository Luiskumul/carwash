<?php require_once dirname(__DIR__).'/action/core.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="img/carwash_icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Dashboard</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <!-- Necesito tener listo jquery -->
    <script src="assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
</head>
<body>
    <div class="wrapper">
        <div class="sidebar" data-color="blue">

            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="javascript:;" class="simple-text">
                      CarWash
                    </a>
                </div>
                <ul class="nav">
                    <li id="navCatalogo">
                        <a class="nav-link" href="catalogo.php">
                            <i class="nc-icon nc-album-2"></i>
                            <p>Catalogo</p>
                        </a>
                    </li>
                    <li id="navClientes">
                        <a class="nav-link" href="clientes.php">
                            <i class="nc-icon nc-notes"></i>
                            <p>Lista de clientes</p>
                        </a>
                    </li>
                    <li id="navFacturas">
                        <a class="nav-link" href="facturas.php">
                            <i class="nc-icon nc-paper-2"></i>
                            <p>Facturas</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class="container-fluid">
                    <a class="navbar-brand" id="navbarBrand" href="#"></a>
                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="home.php">
                                    <span class="no-icon">Cuenta</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="logout.php">
                                    <span class="no-icon">Salir</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
