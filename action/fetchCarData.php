<?php 
require_once __DIR__ . '/core.php';

$carData = [];

if($_SERVER["REQUEST_METHOD"] == "POST"){
    $id = $_POST["id"];

    $sql = "SELECT modelo, descripcion FROM autos WHERE id = '$id'"; 
    $result = $connect->query($sql);

    if ($result->num_rows == 1) {
        $row = $result->fetch_assoc();
        $carData[] = [$row["modelo"], $row["descripcion"]];

    } elseif ($result->num_rows > 1) {
        $carData[] = ["Mas de uno", ""];
    } else {
        $carData[] = ["No encontrado", ""];
    }

    echo json_encode($carData);
}

$connect->close();
?>