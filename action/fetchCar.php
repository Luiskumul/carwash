<?php 
require_once __DIR__ . '/core.php';

$sql = "SELECT * FROM autos"; 
$result = $connect->query($sql);

echo '<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">Foto</th>
      <th scope="col">Model</th>
      <th scope="col">Descripción</th>
      <th scope="col">Vin #</th>
      <th scope="col">Opciones</th>
    </tr>
  </thead>
  <tbody>';

while ($row = $result->fetch_assoc()) { 
	$carID = $row['id'];
	$foto = "upload/".$row['foto'];
	echo "<tr>";
	echo '<td><img src="'. $foto .'" height="70px"></td>';
	echo "<td>" . $row['modelo'] . "</td>";
	echo "<td>" . $row['descripcion'] . "</td>";
	echo "<td>" . $row['vin'] . "</td>";
	// opciones
	echo '<td>
	<a type="button" rel="tooltip" title="Remove" onclick="deleteCar('.$carID.')">
	  <i class="fa fa-remove btn-link btn-danger"></i>
	</a> </td>';
	echo "</tr>";
}
echo '</tbody> </table>';
$connect->close();
?>