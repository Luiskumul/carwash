<?php 
require_once __DIR__ . '/core.php';

if($_SERVER["REQUEST_METHOD"] == "POST"){
	$nombre	=  trim($_POST['nombre']);
	$estado	=  trim($_POST['estado']);
	$ciudad	=  trim($_POST['ciudad']);
	$calle	=  trim($_POST['calle']);
	$zipcode=  trim($_POST['zipcode']);

    if(empty($nombre) || empty($estado) || empty($ciudad) || empty($calle) || empty($zipcode)){
    	echo "Por favor completa todos los datos";
    } else {
        $sql = "INSERT INTO clientes (nombre, estado, ciudad, calle, zipcode) VALUES (?, ?, ?, ?, ?)";
 
        if($stmt = $connect->prepare($sql)){
            $stmt->bind_param("sssss", $nombre, $estado	, $ciudad, $calle, $zipcode);
            
            if($stmt->execute()){
            	echo "Cliente Agregado";
            } else{
                echo "Algo salio mal, Por favor intentalo mas tarde";
            }
        }
         
        $stmt->close();
    }
    
    $connect->close();
}

?>