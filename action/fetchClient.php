<?php 
require_once __DIR__ . '/core.php';

$sql = "SELECT * FROM clientes"; 
$result = $connect->query($sql);

echo '<table class="table table-hover">
  <thead>
      <th>Nombre</th>
      <th>Estado</th>
      <th>Ciudad</th>
      <th>Calle</th>
      <th>ZIP Code</th>
      <th>Opciones</th>
  </thead>
  <tbody>
    <!-- Lista de clientes -->';

while ($row = $result->fetch_assoc()) { 
	$clientId = $row['id'];
	echo "<tr>";
	echo "<td>" . $row['nombre'] . "</td>";
	echo "<td>" . $row['estado'] . "</td>";
	echo "<td>" . $row['ciudad'] . "</td>";
	echo "<td>" . $row['calle'] . "</td>";
	echo "<td>" . $row['zipcode'] . "</td>";
	echo "<td>";
	echo '
	<a type="button" rel="tooltip" title="Remove" onclick="deleteClient('.$clientId.')">
	  <i class="fa fa-remove btn-link btn-danger"></i>
	</a>
	<a type="button" rel="tooltip" title="Edit" onclick="editClient('.$clientId.')">
	  <i class="fa fa-check btn-link btn-info ml-3"></i>
	</a>';
	echo "</td>";
	echo "</tr>";
}
echo '</tbody> </table>';
$connect->close();
?>