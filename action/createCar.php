<?php
require_once __DIR__ . '/core.php';

if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(isset($_FILES["foto"]) && $_FILES["foto"]["error"] == 0){
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
        $filename = $_FILES["foto"]["name"];
        $filetype = $_FILES["foto"]["type"];
        $filesize = $_FILES["foto"]["size"];
    
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Error: Formato de archivo no valido.");
    
        // Tamaño limite 7 MB
        $maxsize = 7 * 1024 * 1024;
        if($filesize > $maxsize) die("Error: El archivo supera el limite de 7MB.");
    
        if(in_array($filetype, $allowed)){
            $uploadFolder = dirname(__DIR__) . "/upload/";
            // Asegurarse de que noexista una imagen con el mismo nombre
            if(file_exists($uploadFolder . $filename)){
                echo $filename . " is already exists.";
            } else{
                $modelo =  trim($_POST['modelo']);
                $vin    =  trim($_POST['vin']);
                $descrip=  trim($_POST['descrip']);

                if (empty($modelo) || empty($vin) || empty($descrip) ) {
                    echo "Por favor completa todos los campos";
                } else {
                    $sql = "INSERT INTO autos (modelo, descripcion, foto, vin) VALUES (?, ?, ?, ?)";
             
                    if($stmt = $connect->prepare($sql)){
                        $stmt->bind_param("ssss", $modelo, $descrip , $filename, $vin);
                        
                        if($stmt->execute()){
                            move_uploaded_file($_FILES["foto"]["tmp_name"], $uploadFolder . $filename);
                            echo "carro agregado";
                        } else{
                            echo "Algo salio mal. Por favor intentalo mas tarde.";
                            exit();
                        }
                    }
                    $stmt->close();
                }
            } 
        } else{
            echo "Error: Al cargar la imagen, Por favor intentalo de nuevo"; 
        }
    } else{
        echo "Error: " . $_FILES["foto"]["error"];
    }
}
?>