<?php
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'password');
define('DB_NAME', 'carwash');

/* Attempt to connect to MySQL database */
$connect = new mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

if ($connect->connect_error) {
    die('Connect Error (' . $connect->connect_errno . ') '
            . $connect->connect_error);
}
