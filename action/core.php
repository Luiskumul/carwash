<?php
session_start();
require_once __DIR__ . '/db_conn.php';

if( !isset($_SESSION['userAdmin'])) {
	header('Location: '. dirname(__DIR__).'/index.php');
}
?>
