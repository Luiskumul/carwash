<?php require_once 'include/header.php'; ?>
<link rel="stylesheet" href="css/suggestions.css">
<link rel="stylesheet" href="https://printjs-4de6.kxcdn.com/print.min.css">
  

<div class="content">

  <div class="container-fluid">
      <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="card-header">
                      <h4 class="card-title">Captura</h4>
                  </div>
                  <div class="card-body">
                      <form autocomplete="off"  id="invoiceForm">
                          <div class="row client-data">
                              <div class="col-md-5 pr-1">
                                  <div class="form-group autocomplete">
                                      <label>Cliente:</label>
                                      <input type="text" id="inClient" class="form-control" placeholder="Nombre del cliente">
                                  </div>
                              </div>
                              <div class="col-md-7 pl-1">
                                  <div class="form-group">
                                      <label>Dirección:</label>
                                      <input type="text" id="clientAddress" class="form-control" placeholder="Client Address">
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                                <div id="table">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Vin#</th>
                                            <th>Modelo</th>
                                            <th>Descripción</th>
                                            <th>Precio</th>
                                            <th>Cantidad</th>
                                            <th>Total</th>
                                            <th id="colRemove">Remove</th>
                                        </tr>
                                        </thead>
                                        <tbody id="list">
                                        </tbody>
                                        <tfoot>
                                          <tr>
                                            <th>Total:</th>
                                            <td>$<span id="sumaTotal"></span></td>
                                          </tr>
                                        </tfoot>
                                    </table>
                                    <span class="table-add float-right mb-3 mr-4" id="addRow">
                                    <a href="#!"  type="button" class="text-success btn-link" data-toggle="modal" data-target="#addCar">
                                    <i class="fa fa-plus fa-2x" aria-hidden="true"></i></a></span>
                                </div>
                            </div>
                          </div>
                          <button type="button" id="print" class="btn btn-primary btn-fill pull-right" onclick="printInvoice()">Generar Factura</button>
                      </form>
                  </div>
              </div>
          </div>
          <div class="col-md-2">
          </div>
      </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="addCar" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="ModalLabel">Agregar Auto</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="addCarForm" autocomplete="off">
            <div class="form-row">
              <div class="form-group col-md-6 autocomplete">
                <label for="carVin">Vin #</label>
                <input type="text" class="form-control" id="carVin" required>
              </div>
              <div class="form-group col-md-6">
                <label for="modelo">Modelo</label>
                <input type="text" class="form-control" id="modelo" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="descripcion">Descripcion</label>
              <input type="text" class="form-control" id="descripcion" readonly>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="precio">Precio</label>
                <input type="number" step="0.01" min="0" class="form-control" id="precio" required>
              </div>
              <div class="form-group col-md-4">
                <label for="cantidad">Cantidad</label>
                <input type="number" min="0" class="form-control" id="cantidad" value="0" required>
              </div>
              <div class="form-group col-md-4">
                <label for="total">Total</label>
                <input type="text" class="form-control" id="total" value="0" readonly>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Agregar</button>
          </form>
        </div>
      </div>
    </div>
  </div>

</div>

<script type="text/javascript" src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
<script type="text/javascript" src="js/facturas.js"></script>
<?php require_once 'include/footer.php'; ?>