<?php 
require_once 'include/header.php';
?>

<div class="content" >
    <!-- Button trigger modal -->
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newClientModal">
    Agregar clientes
  </button>
  <hr>

  <!-- Modal -->
  <div class="modal fade" id="newClientModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="ModalLabel">Nuevo Cliente</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="createClientForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="clientName">Nombre</label>
                <input type="text" class="form-control" id="clientName" name="nombre" placeholder="Nombre" required>
              </div>
              <div class="form-group col-md-6">
                <label for="clientEstado">Estado</label>
                <input type="text" class="form-control" id="clientEstado" name="estado" placeholder="Estado" required>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="clientCiudad">Ciudad</label>
                <input type="text" class="form-control" id="clientCiudad" name="ciudad" placeholder="Ciudad" required>
              </div>
              <div class="form-group col-md-6">
                <label for="clientCalle">Calle</label>
                <input type="text" class="form-control" id="clientCalle" name="calle" placeholder="Calle" required>
              </div>
            </div>
            <div class="form-group">
              <label for="inputAddress2">Zipcode</label>
              <input type="text" class="form-control" id="clientZipcode" name="zipcode" placeholder="ZIP Code" required>
            </div>
            <button type="submit" class="btn btn-primary">Crear</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="card" id="clientList">
      
  </div> <!-- Card para lista de clientes -->
</div>

<script type="text/javascript" src="js/clientes.js"></script>
<?php require_once 'include/footer.php'; ?>