$(document).ready(function() {
    $('#navbarBrand').text("Catalogo");
    $('#navCatalogo').addClass('active');
    mostrarAutos();
    document.getElementById('get_file').onclick = function() {
        document.getElementById('carFoto').click();
    };
    $("#carFoto").change(function () {
        readURL(this);
    });
    $('#createCarForm').submit(function(event) {
        event.preventDefault();
        $('#image_upload_preview').attr('src', "img/placeholder_car.png");
        var image_name = $('#carFoto').val();
        if (image_name == '') {
            alert("Please Select Image");
            return false;
        } else {
            var extension = $('#carFoto').val().split('.').pop().toLowerCase();
            if (jQuery.inArray(extension, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                alert("Invalid Image File");
                $('#carFoto').val('');
                return false;
            } else {
                $.ajax({
                    url: "action/createCar.php",
                    data: new FormData(this),
                    type: "POST",
                    contentType: false,
                    processData: false,
                }).done(function(data, textStatus, jqXHR) {
                    mostrarAutos();
                    document.getElementById("createCarForm").reset();
                    //console.log("Status: " + textStatus + ". ");
                    //console.log(data);
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                });
            }
        }
    });
})

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image_upload_preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function deleteCar(clientid = null) {
    $.ajax({
        url: "action/deleteCar.php",
        data: {
            id: clientid
        },
        type: "GET",
    }).done(function(data, textStatus, jqXHR) {
        mostrarAutos();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (console && console.log) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}

function mostrarAutos() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("autos").innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "action/fetchCar.php", true);
    xhttp.send();
}