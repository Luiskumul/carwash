$(document).ready(function() {
    $('#navClientes').addClass('active');
    $('#navbarBrand').text('Clientes');
    cargarClientes();
    $("#createClientForm").submit(function(event) {
        event.preventDefault();
        var c_nombre = $("#clientName").val();
        var c_estado = $("#clientEstado").val();
        var c_ciudad = $("#clientCiudad").val();
        var c_calle = $("#clientCalle").val();
        var c_zipcode = $("#clientZipcode").val();
        if (c_nombre == "") {
            return
        }
        if (c_estado == "") {
            return
        }
        if (c_ciudad == "") {
            return
        }
        if (c_calle == "") {
            return
        }
        if (c_zipcode == "") {
            return
        }
        $.ajax({
            url: "action/createClient.php",
            data: new FormData(this),
            type: "POST",
            contentType: false,
            processData: false,
        }).done(function(data, textStatus, jqXHR) {
            cargarClientes();
            document.getElementById("createClientForm").reset();
            //console.log("data: " + data);
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log("Error al intentar crear usuario " + textStatus +"--"+ jqXHR +"--" + errorThrown);
        });
    });
})

function deleteClient(clientid = null) {
    $.ajax({
        url: "action/deleteClient.php",
        data: {
            id: clientid
        },
        type: "GET",
    }).done(function(data, textStatus, jqXHR) {
        cargarClientes();
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (console && console.log) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}

function cargarClientes() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("clientList").innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "action/fetchClient.php", true);
    xhttp.send();
}