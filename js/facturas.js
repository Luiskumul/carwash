const Index = 0;
const Value = 1;
const CLIENTE = 2;
const AUTO = 3;
const $tableID = $('#table');

$(document).ready(function () {
	$('#navbarBrand').text("Facturas");
	$('#navFacturas').addClass('active');
	autocomplete(document.getElementById("inClient"), CLIENTE);
	autocomplete(document.getElementById("carVin"), AUTO);

	$tableID.on('click', '.table-remove', function () {
		$(this).parents('tr').detach();
		calculaTotal();
	});

	$('#addCarForm').submit(function (event) {
		event.preventDefault();
		$('#list').append(newTableRow());
		calculaTotal();
		this.reset();
		document.getElementById("carVin").focus();
	});

	var cantidad = document.getElementById("cantidad");
	var precio = document.getElementById("precio");
	var total = document.getElementById("total");
	cantidad.addEventListener("input", function (e) {
		total.value = precio.value * this.value;
	});
	precio.addEventListener("input", function (e) {
		total.value = cantidad.value * this.value;
	});

})

function printInvoice() {
	printJS({printable: "invoiceForm",
		type: 'html',
		ignoreElements:['print','addRow', 'colRemove'],
		scanStyles: false,
		header: '',
		documentTitle: 'Invoice',
		css: "css/factura.css",
	});
}


function newTableRow() {
	var vin = $("#carVin").val()
	var mod = $("#modelo").val()
	var des = $("#descripcion").val()
	var pre = $("#precio").val()
	var can = $("#cantidad").val()
	var tot = $("#total").val()
	var ntrow = `<tr><td>` + vin + `</td><td>` + mod + `</td><td>` + des + `</td><td>` + pre + `</td><td>` + can + `</td><td>` + tot + `</td>
	<td><a href="#" class="table-remove"><i class="fa fa-times btn-link btn-danger"></i></a></td></tr>`;
	return ntrow;
}

function autocomplete(inp, opa) {
	var currentFocus;
	inp.addEventListener("input", function (e) {
		var filephp;
		switch (opa) {
			case CLIENTE:
				filephp = "suggestClient.php";
				break;
			case AUTO:
				filephp = "suggestCar.php";
				break;
		}
		var a, b, i, val = this.value;
		closeAllLists();
		if (!val) { return false; }
		currentFocus = -1;
		a = document.createElement("DIV");
		a.setAttribute("id", this.id + "autocomplete-list");
		a.setAttribute("class", "autocomplete-items");
		this.parentNode.appendChild(a);

		$.ajax({
			type: "POST",
			url: "action/" + filephp,
			data: 'text=' + val,
			success: function (data) {
				var arr = JSON.parse(data);
				//console.log(arr);
				for (i = 0; i < arr.length; i++) {
					var client = arr[i];
					b = document.createElement("DIV");
					b.innerHTML = "<strong>" + client[Value].substr(0, val.length) + "</strong>";
					b.innerHTML += client[Value].substr(val.length);
					b.innerHTML += "<input type='hidden' value='" + client[Value] + "' id='" + client[Index] + "'>";
					b.addEventListener("click", function (e) {
						var elist = this.getElementsByTagName("input")[0];
						inp.value = elist.value;
						switch (opa) {
							case CLIENTE:
								selectClient(elist.id);
								break;
							case AUTO:
								selectCar(elist.id);
								break;
						}
						closeAllLists();
					});
					a.appendChild(b);
				}
			}
		})

	});
	inp.addEventListener("keydown", function (e) {
		var x = document.getElementById(this.id + "autocomplete-list");
		if (x) x = x.getElementsByTagName("div");
		if (e.keyCode == 40) {
			currentFocus++;
			addActive(x);
		} else if (e.keyCode == 38) {
			currentFocus--;
			addActive(x);
		} else if (e.keyCode == 13) {
			e.preventDefault();
			if (currentFocus > -1) {
				if (x) x[currentFocus].click();
			}
		}
	});
	function addActive(x) {
		if (!x) return false;
		removeActive(x);
		if (currentFocus >= x.length) currentFocus = 0;
		if (currentFocus < 0) currentFocus = (x.length - 1);
		x[currentFocus].classList.add("autocomplete-active");
	}
	function removeActive(x) {
		for (var i = 0; i < x.length; i++) {
			x[i].classList.remove("autocomplete-active");
		}
	}
	function closeAllLists(elmnt) {
		var x = document.getElementsByClassName("autocomplete-items");
		for (var i = 0; i < x.length; i++) {
			if (elmnt != x[i] && elmnt != inp) {
				x[i].parentNode.removeChild(x[i]);
			}
		}
	}
	document.addEventListener("click", function (e) {
		closeAllLists(e.target);
	});
}

function selectClient(val) {
	var address = document.getElementById("clientAddress");
	$.ajax({
		type: "POST",
		url: "action/fetchClientData.php",
		data: 'id=' + val,
		success: function (data) {
			address.value = data;
		}
	});
}

function selectCar(val) {
	var modelo = document.getElementById("modelo");
	var descrip = document.getElementById("descripcion");
	$.ajax({
		type: "POST",
		url: "action/fetchCarData.php",
		data: 'id=' + val,
		success: function (data) {
			var objdata = JSON.parse(data)[0];
			//console.log(objdata);
			modelo.value = objdata[0];
			descrip.value = objdata[1];
			document.getElementById("precio").focus();
		}
	});

}

function calculaTotal() {
	var lista = $("#list")[0].children;
	console.log(lista.length);
	var sumaTotal = 0;
	for (let idx = 0; idx < lista.length; idx++) {
		const val = lista[idx].children[5].innerHTML;
		console.log(val);
		sumaTotal += parseFloat(val);
	}
	$("#sumaTotal")[0].innerHTML = sumaTotal;
	console.log("suma total: " + sumaTotal);
}
