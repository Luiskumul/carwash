<?php
require_once 'action/db_conn.php';

session_start();

if(isset($_SESSION['userAdmin'])) {
	header('location: dashboard/home.php');
}

$errors = array();

// Define variables and initialize with empty values
$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){

    // Validate username
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter a username.";
				$errors[] = $username_err;
    } else{
        // Prepare a select statement
        $sql = "SELECT user_id FROM users WHERE username = ?";

        if($stmt = $connect->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bind_param("s", $param_username);

            // Set parameters
            $param_username = trim($_POST["username"]);

            // Attempt to execute the prepared statement
            if($stmt->execute()){
                // store result
                $stmt->store_result();

                if($stmt->num_rows == 1){
                    $username_err = "Username " . $param_username . " is already taken.";
										$errors[] = $username_err;
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }

        // Close statement
        $stmt->close();
    }

    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";
				$errors[] = $password_err;
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have atleast 6 characters.";
				$errors[] = $password_err;
    } else{
        $password = trim($_POST["password"]);
    }

    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm password.";
				$errors[] = $confirm_password_err;
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
						$errors[] = $confirm_password_err;
        }
    }

    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){

        // Prepare an insert statement
        $sql = "INSERT INTO users (username, password) VALUES (?, ?)";

        if($stmt = $connect->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bind_param("ss", $param_username, $param_password);

            // Set parameters
            $param_username = $username;
            $param_password = md5($password); // Creates a password hash

            // Attempt to execute the prepared statement
            if($stmt->execute()){
                // Redirect to login page
                header("location: index.php");
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }

        // Close statement
        $stmt->close();
    }

    // Close connection
    $connect->close();
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>Car Wash</title>
    <meta charset="utf-8">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body>

	<nav>
	  <span class="fadeIn">Carwash admin - Register</span>
	</nav>

    <div class="wrapper fadeInDown">
      <div id="formContent">
        <!-- Tabs Titles -->
        <h2>Register</h2>

				<div class="messages px-2">
					<?php if($errors) {
						foreach ($errors as $key => $value) {
							echo '<div class="alert alert-warning" role="alert">
							<i class="glyphicon glyphicon-exclamation-sign"></i>
							'.$value.'</div>';
							}
						} ?>
				</div>

        <!-- Icon -->
        <div class="fadeIn first">
          <img src="img/car-wash.jpg" id="icon" alt="car-wash icon" />
        </div>

        <!-- Login Form -->
        <form action="register.php" method="post">
          <input type="text" id="login" class="fadeIn second" name="username" placeholder="username" required>
          <input type="password" id="password" min="5" class="fadeIn third" name="password" placeholder="password" required>
          <input type="password" id="password-con" min="5" class="fadeIn third" name="confirm_password" placeholder="confirm password" required>
          <input type="submit" class="fadeIn fourth" value="Register">
        </form>

        <!-- Log in -->
        <div id="formFooter">
          <a class="underlineHover" href="index.php">Log in</a>
        </div>

      </div>
    </div>
</body>
</html>

