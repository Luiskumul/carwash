<?php 
require_once 'include/header.php';
?>

<div class="content">

  <!-- Button trigger modal -->
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#carModal">
    Agregar autos
  </button>
  <hr>

  <!-- Modal -->
  <div class="modal fade" id="carModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="ModalLabel">Agregar Auto</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="createCarForm">
            <div class="form-group">
              <input type="button" class="btn mr-2" id="get_file" value="Foto">
              <input type="file" id="carFoto" name="foto" style="display: none;">
              <img id="image_upload_preview" src="img/placeholder_car.png" height="120px" alt="car image" />
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="carModelo">Modelo</label>
                <input type="text" class="form-control" id="carModelo" name="modelo" placeholder="Modelo">
              </div>
              <div class="form-group col-md-6">
                <label for="carVin">Vin #</label>
                <input type="text" class="form-control" id="carVin" name="vin" placeholder="Vin #">
              </div>
            </div>
            <div class="form-group">
              <label for="carDescrip">Descripcion</label>
              <input type="text" class="form-control" id="carDescrip" name="descrip" placeholder="Descripción">
            </div>
            <button type="submit" class="btn btn-primary">Agregar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
	
  <div id="autos">
    <!--Catalogo de Autos-->
  </div>
</div>

<script type="text/javascript" src="js/catalogo.js"></script>
<?php require_once 'include/footer.php'; ?>
