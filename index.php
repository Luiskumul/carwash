<?php
require_once 'action/db_conn.php';

session_start();

if(isset($_SESSION['userAdmin'])) {
	header('location: home.php');
}

$errors = array();

if($_POST) {

	$username = trim($_POST["username"]);
	$password = trim($_POST['password']);

	$sql = "SELECT * FROM users WHERE username = '$username'";
	$result = $connect->query($sql);

	if($result->num_rows == 1) {
		$password = md5($password);
		// exists
		$mainSql = "SELECT * FROM users WHERE username = '$username' AND password = '$password'";
		$mainResult = $connect->query($mainSql);

		if($mainResult->num_rows == 1) {
			$value = $mainResult->fetch_assoc();
			$userAdmin = $value['username'];

			// set session
			$_SESSION['userAdmin'] = $userAdmin;

			header('location: home.php');
		} else{

			$errors[] = "The username and password combination you entered is not recognized or does not exist";
		} // /else
	} else {
		$errors[] = "Username does not exists";
	} // /else

} // /if $_POST
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>Car Wash</title>
    <meta charset="utf-8">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body>

	<nav>
	  <span class="fadeIn">Carwash admin - Log In</span>
	</nav>

    <div class="wrapper fadeInDown">
      <div id="formContent">
        <!-- Tabs Titles -->
        <h2>Log in</h2>

				<div class="messages px-2">
					<?php if($errors) {
						foreach ($errors as $key => $value) {
							echo '<div class="alert alert-warning" role="alert">
							'.$value.'</div>';
							}
						} ?>
				</div>

        <!-- Icon -->
        <div class="fadeIn first">
          <img src="img/car-wash.jpg" id="icon" alt="car-wash icon" />
        </div>

        <!-- Login Form -->
        <form action="<?=htmlentities($_SERVER['PHP_SELF'])?>" method="post">
          <input type="text" id="login" class="fadeIn second" name="username" placeholder="username" required>
          <input type="password" id="password" min="5" class="fadeIn third" name="password" placeholder="password" required>
          <input type="submit" class="fadeIn fourth" value="Log In">
        </form>

        <!-- Register -->
        <div id="formFooter">
          <a class="underlineHover" href="register.php">Register</a>
        </div>

      </div>
    </div>
</body>
</html>
